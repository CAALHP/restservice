﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using caalhp.Core.Contracts;
using caalhp.Core.Events;
using caalhp.Core.Utils.Helpers;
using caalhp.Core.Utils.Helpers.Serialization;

namespace Crip2CaalhpService
{
    public class NfcController : ApiController
    {
        private readonly string _name;
        private readonly int _processId;
        private readonly IServiceHostCAALHPContract _host;

        public NfcController(string name, IServiceHostCAALHPContract host, int processId)
        {
            _name = name;
            _host = host;
            _processId = processId;
        }

        [HttpPost]
        [ActionName("nfc/tag")]
        public HttpResponseMessage PostNfcTag(NfcTag tag)
        {
            try
            {
                var foundNfcTagEvent = new RFIDFoundEvent
                {
                    
                    Tag = tag.Id,
                    CallerName = _name,
                    CallerProcessId = _processId
                };
                _host.Host.ReportEvent(EventHelper.CreateEvent(SerializationType.Json, foundNfcTagEvent));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            //implementation – return a code 200 if all is well
            var response = new HttpResponseMessage(HttpStatusCode.OK);
            Console.WriteLine("Found tag: " + tag.Id);
            return response;
        }
    }
}
