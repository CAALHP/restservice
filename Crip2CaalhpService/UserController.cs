﻿using System;
using System.IO;
using System.Media;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using caalhp.Core.Contracts;
using caalhp.Core.Events;
using caalhp.Core.Utils.Helpers;
using caalhp.Core.Utils.Helpers.Serialization;
using crip2caalhp_events;

namespace Crip2CaalhpService
{
    public class UserController : ApiController
    {

        private readonly string _name;
        private readonly int _processId;
        private readonly IServiceHostCAALHPContract _host;

        public UserController(string name, IServiceHostCAALHPContract host, int processId)
        {
            _name = name;
            _host = host;
            _processId = processId;
        }

        //[POST("auth")]
        [HttpPost]
        [ActionName("user/auth")]
        public HttpResponseMessage PostAuth(AuthToken auth)
        {
            try
            {
                if (IsAuthValid(auth))
                {
                    ReportFoundId(auth);
                }
                else
                {
                    SendAuthError();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            //implementation – return a code 200 if all is well
            var response = new HttpResponseMessage(HttpStatusCode.OK);
            PlayCompleteSound();
            return response;
        }

        private static void PlayCompleteSound()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var dir = Path.GetDirectoryName(assembly.Location);
            if (dir == null) return;
            var soundfile = Path.Combine(dir, "complete.wav");
            var player = new SoundPlayer(soundfile);
            player.Play();
        }

        private void ReportFoundId(AuthToken auth)
        {
            var foundIdEvent = new FoundIdFromCripEvent
            {
                AuthType = auth.AuthType,
                Id = auth.Id,
                CallerName = _name,
                CallerProcessId = _processId
            };
            _host.Host.ReportEvent(EventHelper.CreateEvent(SerializationType.Json, foundIdEvent,
                "crip2caalhp-events"));
        }

        private static bool IsAuthValid(AuthToken auth)
        {
            return !String.IsNullOrWhiteSpace(auth.Id) && !String.IsNullOrWhiteSpace(auth.AuthType);
        }

        private void SendAuthError()
        {
            var error = new ErrorEvent
            {
                CallerName = _name,
                CallerProcessId = _processId,
                ErrorMessage = "User not authenticated!"
            };
            _host.Host.ReportEvent(EventHelper.CreateEvent(SerializationType.Json, error));
        }
    }
}