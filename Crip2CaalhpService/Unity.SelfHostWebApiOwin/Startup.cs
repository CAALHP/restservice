using System;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Filters;
using Crip2CaalhpService.Unity.SelfHostWebApiOwin;
using Microsoft.Owin;
using Microsoft.Owin.Hosting;
using Microsoft.Practices.Unity;
using Owin;
using Unity.SelfHostWebApiOwin;

[assembly: OwinStartup(typeof(Startup))]

namespace Crip2CaalhpService.Unity.SelfHostWebApiOwin
{
    public class Startup
    {
        private static readonly IUnityContainer Container = UnityHelpers.GetConfiguredContainer();

        // Your startup logic
        public static void StartServer()
        {
            //var baseAddress = ConfigurationManager.AppSettings.Get("baseaddress");
            var startup = Container.Resolve<Startup>();
            var options = new StartOptions();
            // Listen on all local interfaces:
            options.Urls.Add("http://+:60773/");
            // And for http over ssl:
            options.Urls.Add("https://+:60774/");
            var webApplication = WebApp.Start(options, startup.Configuration);

            try
            {
                Console.WriteLine("Started...");

                Console.ReadKey();
            }
            finally
            {
                webApplication.Dispose();
            }
        }

        // This code configures Web API. The Startup class is specified as a type
        // parameter in the WebApp.Start method.
        public void Configuration(IAppBuilder appBuilder)
        {
            // Configure Web API for self-host. 
            var config = new HttpConfiguration
            {
                DependencyResolver = new UnityDependencyResolver(UnityHelpers.GetConfiguredContainer())
            };

            // Add Unity DependencyResolver

            // Add Unity filters provider
            RegisterFilterProviders(config);

            config.Routes.MapHttpRoute(
                name: "Default",
                routeTemplate: "{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
                );
            //config.Routes.MapHttpRoute(
            //    name: "PostAuth",
            //    routeTemplate: "{controller}/methodtwo/{directory}/{report}",
            //    defaults: new { directory = RouteParameter.Optional, report = RouteParameter.Optional }
            //    );
            //config.Routes..MapHttpAttributeRoutes(
            //    cfg => cfg.AddRoutesFromAssembly(Assembly.GetExecutingAssembly()));


            // Web API routes
            config.MapHttpAttributeRoutes();

            appBuilder.UseWebApi(config);
        }

        private static void RegisterFilterProviders(HttpConfiguration config)
        {
            // Add Unity filters provider
            var providers = config.Services.GetFilterProviders().ToList();
            config.Services.Add(typeof(IFilterProvider), new WebApiUnityActionFilterProvider(UnityHelpers.GetConfiguredContainer()));
            var defaultprovider = providers.First(p => p is ActionDescriptorFilterProvider);
            config.Services.Remove(typeof(IFilterProvider), defaultprovider);
        }

    }
}
