﻿namespace Crip2CaalhpService
{
    public class NfcTag
    {
        //{"id":"nfc-tag-id","key":"value"} where nfc-tag-id is a string with the ID of the nfc tag.
        public string Id { get; set; }
        public string Pin { get; set; }
    }
}