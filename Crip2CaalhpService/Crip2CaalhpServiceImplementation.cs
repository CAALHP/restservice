﻿
using System.Collections.Generic;
using System.Diagnostics;
using caalhp.Core.Contracts;
using System;
using caalhp.IcePluginAdapters;
using Crip2CaalhpService.Unity.SelfHostWebApiOwin;
using Microsoft.Practices.Unity;

namespace Crip2CaalhpService
{
    public class Crip2CaalhpServiceImplementation : IServiceCAALHPContract
    {
        private IServiceHostCAALHPContract _host;
        private int _processId;
        //private readonly Uri _baseAddress;
        //private IDisposable _server;
        //private readonly HttpSelfHostConfiguration _config;

        public Crip2CaalhpServiceImplementation()
        {
            //_baseAddress = address;
            //var address = ConfigurationManager.AppSettings.Get("baseaddress");
            //_baseAddress = new Uri(address);
            //_config = new HttpSelfHostConfiguration(_baseAddress);
            
        }

        public void Notify(KeyValuePair<string, string> notification)
        {

        }

        public string GetName()
        {
            return "Rest Service";
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            Environment.Exit(0);
        }

        public void Initialize(IServiceHostCAALHPContract hostObj, int processId)
        {
            try
            {
                _host = hostObj;
                _processId = processId;
                //_host.SubscribeToEvents(_processId);
                Console.WriteLine("About to start the Unity framework and Owin selfhost");
                Start();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error when activating Start() : " + e.Message);
            }

        }

        public void Start()
        {
            
            try
            {
                var unity = UnityHelpers.GetConfiguredContainer();
                
                unity.RegisterInstance(_host);
                unity.RegisterInstance(_processId);
                unity.RegisterType<DeviceController>(new InjectionConstructor(_host, _processId));
                unity.RegisterInstance(this);
                unity.RegisterType<UserController>(new InjectionConstructor(GetName(), _host, _processId));
                unity.RegisterType<NfcController>(new InjectionConstructor(GetName(), _host, _processId));
                unity.RegisterType<IServiceHostCAALHPContract, ServiceHostToCAALHPContractAdapter>(
                    new PerResolveLifetimeManager());
                Startup.StartServer();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        public void Stop()
        {
            // Close the ServiceHost.
            //_server.Dispose();
        }
    }
}
