﻿using System;
using caalhp.IcePluginAdapters;
using Crip2CaalhpService;

namespace Crip2CaalhpServer
{
    class Program
    {
        static void Main(string[] args)
        {
            const string endpoint = "localhost";
            Console.WriteLine("starting Crip2CaalhpService...");
            Crip2CaalhpServiceImplementation implementation = null;
            ServiceAdapter adapter;
            try
            {
                implementation = new Crip2CaalhpServiceImplementation();
                adapter = new ServiceAdapter(endpoint, implementation);
                Console.WriteLine("Crip2CaalhpService running");
                Console.WriteLine("press <enter> to exit...");
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                //Connection to host probably failed
                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }
            
        }
    }
}
